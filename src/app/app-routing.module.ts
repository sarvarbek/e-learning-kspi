import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {WelcomeComponent} from "./welcome/welcome.component";
import {RoutesEnum} from "./routes.enum";
import {AboutComponent} from "./about/about.component";

const routes: Routes = [
  {
    path: '',
    redirectTo: RoutesEnum.WELCOME,
    pathMatch: 'full'
  },
  {
    path: RoutesEnum.WELCOME,
    component: WelcomeComponent
  },
  {
    path: RoutesEnum.LECTURES,
    loadChildren: () => import('./lections/lections.module').then(m => m.LectionsModule)
  },
  {
    path: RoutesEnum.PRACTICES,
    loadChildren: () => import('./practices/practices.module').then(m => m.PracticesModule)
  },
  {
    path: RoutesEnum.LABORATORIES,
    loadChildren: () => import('./laboratories/laboratories.module').then(m => m.LaboratoriesModule)
  },
  {
    path: RoutesEnum.ABOUT,
    component: AboutComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
