import {RoutesEnum} from "./routes.enum";

export class MenuItems {
    static items = [
        {
            title: 'LECTURE.TITLE',
            icon: 'dashboard',
            isOpened: false,
            pages: [
                {
                    title: 'LECTURE.FIRST',
                    path: `${RoutesEnum.LECTURES}/${RoutesEnum.FIRST}`
                },
                {
                    title: 'LECTURE.SECOND',
                    path: `${RoutesEnum.LECTURES}/${RoutesEnum.SECOND}`
                },
                {
                    title: `LECTURE.THIRD`,
                    path: `${RoutesEnum.LECTURES}/${RoutesEnum.THIRD}`

                },
                {
                    title: `LECTURE.FOURTH`,
                    path: `${RoutesEnum.LECTURES}/${RoutesEnum.FOURTH}`

                },
                {
                    title: `LECTURE.FIFTH`,
                    path: `${RoutesEnum.LECTURES}/${RoutesEnum.FIFTH}`
                },
                {
                    title: `LECTURE.SIXTH`,
                    path: `/${RoutesEnum.LECTURES}/${RoutesEnum.SIXTH}`
                },
                {
                    title: `LECTURE.SEVENTH`,
                    path: `/${RoutesEnum.LECTURES}/${RoutesEnum.SEVENTH}`
                },
                {
                    title: `LECTURE.EIGHTH`,
                    path: `/${RoutesEnum.LECTURES}/${RoutesEnum.EIGHTH}`
                },
                {
                    title: `LECTURE.NINTH`,
                    path: `/${RoutesEnum.LECTURES}/${RoutesEnum.NINTH}`
                },
                {
                    title: `LECTURE.TENTH`,
                    path: `/${RoutesEnum.LECTURES}/${RoutesEnum.TENTH}`
                },
                {
                    title: `LECTURE.ELEVENTH`,
                    path: `/${RoutesEnum.LECTURES}/${RoutesEnum.ELEVENTH}`
                },
                {
                    title: `LECTURE.TWELFTH`,
                    path: `/${RoutesEnum.LECTURES}/${RoutesEnum.TWELFTH}`
                }

            ]
        },
        {
            title: `PRACTICES.TITLE`,
            icon: 'dashboard',
            isOpened: false,
            pages: [
                {
                    title: `PRACTICES.FIRST`,
                    path: `${RoutesEnum.PRACTICES}/${RoutesEnum.FIRST}`
                },
                {
                    title: `PRACTICES.SECOND`,
                    path: `${RoutesEnum.PRACTICES}/${RoutesEnum.SECOND}`
                },
                {
                    title: `PRACTICES.THIRD`,
                    path: `${RoutesEnum.PRACTICES}/${RoutesEnum.THIRD}`
                },
                {
                    title: `PRACTICES.FOURTH`,
                    path: `${RoutesEnum.PRACTICES}/${RoutesEnum.FOURTH}`
                },
                {
                    title: `PRACTICES.FIFTH`,
                    path: `${RoutesEnum.PRACTICES}/${RoutesEnum.FIFTH}`
                },
                {
                    title: `PRACTICES.SIXTH`,
                    path: `${RoutesEnum.PRACTICES}/${RoutesEnum.SIXTH}`
                },
                {
                    title: `PRACTICES.SEVENTH`,
                    path: `${RoutesEnum.PRACTICES}/${RoutesEnum.SEVENTH}`
                },
                {
                    title: `PRACTICES.EIGHTH`,
                    path: `${RoutesEnum.PRACTICES}/${RoutesEnum.EIGHTH}`
                }
            ]
        },
        {
            title: `LABORATORIES.TITLE`,
            icon: 'dashboard',
            isOpened: false,
            pages: [
                {
                    title: `LABORATORIES.FIRST`,
                    path: `${RoutesEnum.LABORATORIES}/${RoutesEnum.FIRST}`
                },
                {
                    title: `LABORATORIES.SECOND`,
                    path: `${RoutesEnum.LABORATORIES}/${RoutesEnum.SECOND}`
                },
                {
                    title: `LABORATORIES.THIRD`,
                    path: `${RoutesEnum.LABORATORIES}/${RoutesEnum.THIRD}`
                },
                {
                    title: `LABORATORIES.FOURTH`,
                    path: `${RoutesEnum.LABORATORIES}/${RoutesEnum.FOURTH}`
                },
                {
                    title: `LABORATORIES.FIFTH`,
                    path: `${RoutesEnum.LABORATORIES}/${RoutesEnum.FIFTH}`
                },
                {
                    title: `LABORATORIES.SIXTH`,
                    path: `${RoutesEnum.LABORATORIES}/${RoutesEnum.SIXTH}`
                },
                {
                    title: `LABORATORIES.SEVENTH`,
                    path: `${RoutesEnum.LABORATORIES}/${RoutesEnum.SEVENTH}`
                },
                {
                    title: `LABORATORIES.EIGHTH`,
                    path: `${RoutesEnum.LABORATORIES}/${RoutesEnum.EIGHTH}`
                }
            ]
        },
        {
            title: `ABOUT.TITLE`,
            icon: 'dashboard',
            isOpened: false,
            pages: [
                {
                    title: `ABOUT.MANUAL`,
                    path: RoutesEnum.ABOUT
                }
            ]
        }]
}
