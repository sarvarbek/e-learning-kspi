import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {FirstLaboratoryComponent} from "./pages/first-laboratory/first-laboratory.component";
import {SecondLaboratoryComponent} from "./pages/second-laboratory/second-laboratory.component";
import {ThirdLaboratoryComponent} from "./pages/third-laboratory/third-laboratory.component";
import {FourthLaboratoryComponent} from "./pages/fourth-laboratory/fourth-laboratory.component";
import {FifthLaboratoryComponent} from "./pages/fifth-laboratory/fifth-laboratory.component";
import {SixthLaboratoryComponent} from "./pages/sixth-laboratory/sixth-laboratory.component";
import {SeventhLaboratoryComponent} from "./pages/seventh-laboratory/seventh-laboratory.component";
import {EighthLaboratoryComponent} from "./pages/eighth-laboratory/eighth-laboratory.component";
import {RoutesEnum} from "../routes.enum";
import {LaboratoriesComponent} from "./laboratories.component";


const routes: Routes = [
    {
        path: '',
        component: LaboratoriesComponent,
        children: [
            {
                path: '',
                redirectTo: RoutesEnum.FIRST,
                pathMatch: 'full'
            },
            {
                path: RoutesEnum.FIRST,
                component: FirstLaboratoryComponent
            },
            {
                path: RoutesEnum.SECOND,
                component: SecondLaboratoryComponent
            },
            {
                path: RoutesEnum.THIRD,
                component: ThirdLaboratoryComponent
            },

            {
                path: RoutesEnum.FOURTH,
                component: FourthLaboratoryComponent
            },
            {
                path: RoutesEnum.FIFTH,
                component: FifthLaboratoryComponent
            },
            {
                path: RoutesEnum.SIXTH,
                component: SixthLaboratoryComponent
            },
            {
                path: RoutesEnum.SEVENTH,
                component: SeventhLaboratoryComponent
            },
            {
                path: RoutesEnum.EIGHTH,
                component: EighthLaboratoryComponent
            }


        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LaboratoriesRoutingModule {
}
