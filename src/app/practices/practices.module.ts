import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {PracticesRoutingModule} from './practices-routing.module';
import {FirstPracticeComponent} from './pages/first-practice/first-practice.component';
import {SecondPracticeComponent} from './pages/second-practice/second-practice.component';
import {ThirdPracticeComponent} from './pages/third-practice/third-practice.component';
import {FourthPracticeComponent} from './pages/fourth-practice/fourth-practice.component';
import {FifthPracticeComponent} from './pages/fifth-practice/fifth-practice.component'
import {PracticesComponent} from "./practices.component";
import { SixthPracticeComponent } from './pages/sixth-practice/sixth-practice.component';
import { SeventhPracticeComponent } from './pages/seventh-practice/seventh-practice.component';
import { EighthPracticeComponent } from './pages/eighth-practice/eighth-practice.component';
import {NzTypographyModule} from "ng-zorro-antd/typography";
import {NzImageModule} from "ng-zorro-antd/image";


@NgModule({
  declarations: [
    PracticesComponent,
    FirstPracticeComponent,
    FirstPracticeComponent,
    SecondPracticeComponent,
    ThirdPracticeComponent,
    FourthPracticeComponent,
    FifthPracticeComponent,
    SixthPracticeComponent,
    SeventhPracticeComponent,
    EighthPracticeComponent
  ],
    imports: [
        CommonModule,
        PracticesRoutingModule,
        NzTypographyModule,
        NzImageModule
    ]
})
export class PracticesModule {
}
