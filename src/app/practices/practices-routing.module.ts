import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {FirstPracticeComponent} from "./pages/first-practice/first-practice.component";
import {SecondPracticeComponent} from "./pages/second-practice/second-practice.component";
import {ThirdPracticeComponent} from "./pages/third-practice/third-practice.component";
import {FourthPracticeComponent} from "./pages/fourth-practice/fourth-practice.component";
import {FifthPracticeComponent} from "./pages/fifth-practice/fifth-practice.component";
import {SixthPracticeComponent} from "./pages/sixth-practice/sixth-practice.component";
import {SeventhPracticeComponent} from "./pages/seventh-practice/seventh-practice.component";
import {EighthPracticeComponent} from "./pages/eighth-practice/eighth-practice.component";
import {RoutesEnum} from "../routes.enum";
import {PracticesComponent} from "./practices.component";

const routes: Routes = [
    {
        path: '',
        component: PracticesComponent,
        children: [
            {
                path: '',
                redirectTo: RoutesEnum.FIRST,
                pathMatch: 'full'
            },
            {
                path: RoutesEnum.FIRST,
                component: FirstPracticeComponent
            },
            {
                path: RoutesEnum.SECOND,
                component: SecondPracticeComponent
            },
            {
                path: RoutesEnum.THIRD,
                component: ThirdPracticeComponent
            },

            {
                path: RoutesEnum.FOURTH,
                component: FourthPracticeComponent
            },
            {
                path: RoutesEnum.FIFTH,
                component: FifthPracticeComponent
            },
            {
                path: RoutesEnum.SIXTH,
                component: SixthPracticeComponent
            },
            {
                path: RoutesEnum.SEVENTH,
                component: SeventhPracticeComponent
            },
            {
                path: RoutesEnum.EIGHTH,
                component: EighthPracticeComponent
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PracticesRoutingModule {
}
