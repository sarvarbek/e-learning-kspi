import {Injectable} from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class ThemeService {
  static THEME_KEY = 'kspi_theme';
  static DARK = 'dark';
  static DEFAULT = 'default';

  currentTheme = ThemeService.DEFAULT;

  constructor() {}

  private static reverseTheme(theme: string): string {
    return theme === ThemeService.DARK ? ThemeService.DEFAULT : ThemeService.DARK;
  }

  private static removeUnusedTheme(theme: string) {
    document.documentElement.classList.remove(theme);
    const removedThemeStyle = document.getElementById(theme);
    if (removedThemeStyle) {
      document.head.removeChild(removedThemeStyle);
    }
  }

  private loadCss(href: string, id: string): Promise<Event> {
    return new Promise((resolve, reject) => {
      const style = document.createElement('link');
      style.rel = 'stylesheet';
      style.href = href;
      style.id = id;
      style.onload = resolve;
      style.onerror = reject;
      document.head.append(style);
    });
  }

  public loadTheme(firstLoad = true): Promise<Event> {
    const theme = this.currentTheme;
    localStorage.setItem(ThemeService.THEME_KEY, theme);
    if (firstLoad) {
      document.documentElement.classList.add(theme);
    }
    return new Promise<Event>((resolve, reject) => {
      this.loadCss(`${theme}.css`, theme).then(
          (e) => {
            if (!firstLoad) {
              document.documentElement.classList.add(theme);
            }
            ThemeService.removeUnusedTheme(ThemeService.reverseTheme(theme));
            resolve(e);
          },
          (e) => reject(e)
      );
    });
  }

  public toggleTheme(): Promise<Event> {
    this.currentTheme = ThemeService.reverseTheme(this.currentTheme);
    return this.loadTheme(false);
  }
}
