import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LectionsComponent} from "./lections.component";
import {FirstLectionComponent} from "./pages/first-lection/first-lection.component";
import {SecondLectionComponent} from "./pages/second-lection/second-lection.component";
import {FourthLectionComponent} from "./pages/fourth-lection/fourth-lection.component";
import {FifthLectionComponent} from "./pages/fifth-lection/fifth-lection.component";
import {SixthLectionComponent} from "./pages/sixth-lection/sixth-lection.component";
import {SeventhLectionComponent} from "./pages/seventh-lection/seventh-lection.component";
import {EighthLectionComponent} from "./pages/eighth-lection/eighth-lection.component";
import {NinthLectionComponent} from "./pages/ninth-lection/ninth-lection.component";
import {TenthLectionComponent} from "./pages/tenth-lection/tenth-lection.component";
import {EleventhLectionComponent} from "./pages/eleventh-lection/eleventh-lection.component";
import {TwelfthLectionComponent} from "./pages/twelfth-lection/twelfth-lection.component";
import {ThirdLectionComponent} from "./pages/third-lection/third-lection.component";
import {RoutesEnum} from "../routes.enum";

const routes: Routes = [
  {
    path: '',
    component: LectionsComponent,
    children: [
      {
        path: '',
        redirectTo: RoutesEnum.FIRST,
        pathMatch: 'full'
      },
      {
        path: RoutesEnum.FIRST,
        component: FirstLectionComponent
      },
      {
        path: RoutesEnum.SECOND,
        component: SecondLectionComponent
      },
      {
        path: RoutesEnum.THIRD,
        component: ThirdLectionComponent
      },

      {
        path: RoutesEnum.FOURTH,
        component: FourthLectionComponent
      },
      {
        path: RoutesEnum.FIFTH,
        component: FifthLectionComponent
      },
      {
        path: RoutesEnum.SIXTH,
        component: SixthLectionComponent
      },
      {
        path: RoutesEnum.SEVENTH,
        component: SeventhLectionComponent
      },
      {
        path: RoutesEnum.EIGHTH,
        component: EighthLectionComponent
      },
      {
        path:  RoutesEnum.NINTH,
        component: NinthLectionComponent
      },
      {
        path: RoutesEnum.TENTH,
        component: TenthLectionComponent
      },
      {
        path: RoutesEnum.ELEVENTH,
        component: EleventhLectionComponent
      },
      {
        path: RoutesEnum.TWELFTH,
        component: TwelfthLectionComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LectionsRoutingModule {
}
