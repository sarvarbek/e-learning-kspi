import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {LectionsRoutingModule} from './lections-routing.module';
import {FirstLectionComponent} from './pages/first-lection/first-lection.component';
import {SecondLectionComponent} from './pages/second-lection/second-lection.component';
import {ThirdLectionComponent} from './pages/third-lection/third-lection.component';
import {LectionsComponent} from "./lections.component";
import { FourthLectionComponent } from './pages/fourth-lection/fourth-lection.component';
import { FifthLectionComponent } from './pages/fifth-lection/fifth-lection.component';
import { SixthLectionComponent } from './pages/sixth-lection/sixth-lection.component';
import { SeventhLectionComponent } from './pages/seventh-lection/seventh-lection.component';
import { EighthLectionComponent } from './pages/eighth-lection/eighth-lection.component';
import { NinthLectionComponent } from './pages/ninth-lection/ninth-lection.component';
import { TenthLectionComponent } from './pages/tenth-lection/tenth-lection.component';
import { EleventhLectionComponent } from './pages/eleventh-lection/eleventh-lection.component';
import { TwelfthLectionComponent } from './pages/twelfth-lection/twelfth-lection.component';
import {NzTypographyModule} from "ng-zorro-antd/typography";
import {NzImageModule} from "ng-zorro-antd/image";


@NgModule({
  declarations: [
    LectionsComponent,
    FirstLectionComponent,
    SecondLectionComponent,
    ThirdLectionComponent,
    FourthLectionComponent,
    FifthLectionComponent,
    SixthLectionComponent,
    SeventhLectionComponent,
    EighthLectionComponent,
    NinthLectionComponent,
    TenthLectionComponent,
    EleventhLectionComponent,
    TwelfthLectionComponent,
  ],
    imports: [
        CommonModule,
        LectionsRoutingModule,
        NzTypographyModule,
        NzImageModule
    ]
})
export class LectionsModule {
}
