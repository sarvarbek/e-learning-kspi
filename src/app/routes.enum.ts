export class RoutesEnum {
    static WELCOME = 'welcome';
    static LABORATORIES = 'laboratories';
    static PRACTICES = 'practices';
    static LECTURES = 'lectures';
    static ABOUT = 'about'

    static FIRST = 'first';
    static SECOND = 'second';
    static THIRD = 'third';
    static FOURTH = 'fourth';
    static FIFTH = 'fifth';
    static SIXTH = 'sixth';
    static SEVENTH = 'seventh';
    static EIGHTH = 'eighth';
    static NINTH = 'ninth';
    static TENTH = 'tenth';
    static ELEVENTH = 'eleventh';
    static TWELFTH = 'twelfth';
    static MANUAL = 'manual';


}
